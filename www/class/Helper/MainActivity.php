<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 09.11.18
 * Time: 17:57
 */

namespace B24\Helper;

class MainActivity
{

    public static function SetHeader()
    {
        $protocol='https';
        $host=explode(':',$_SERVER['HTTP_HOST']);
        $host=$host[0];
        define('BP_APP_HANDLER',$protocol.'://'.$host.$_SERVER['REQUEST_URI']);
        define('BP_APP_HANDLER_HANDLER',$protocol.'://'.$host.'/activity/addtask/handler/?'.$_SERVER['QUERY_STRING']);
        define('BP_APP_HANDLER_HANDLER_UPDATE',$protocol.'://'.$host.'/activity/addtask/updatehandler/?'.$_SERVER['QUERY_STRING']);
        header('Content-Type: text/html; charset=UTF-8');

        //echo "<pre>";print_r($_SERVER);echo "</pre>";

    }

    public static function show()
    {
        self::SetHeader();
        require_once($_SERVER["DOCUMENT_ROOT"].'/class/View/Activity/AddTask.php');
    }



}