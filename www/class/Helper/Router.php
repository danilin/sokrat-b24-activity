<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 08.11.2018
 * Time: 23:55
 */

namespace B24\Helper;

use FastRoute;
use B24\Helper\Activity;

class Router
{

    public static function init()
    {
        header('Content-type: application/json');
        try {
            self::initMain();
        } catch (\Exception $e) {
            MainTools::sendError($e->getMessage());
        }
    }

    public static function initMain()
    {

        $dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
            $r->addGroup('/activity', function (FastRoute\RouteCollector $r) {

                $r->addGroup('/addtask', function (FastRoute\RouteCollector $r) {
                    $r->addRoute([
                        'GET',
                        'POST',
                    ], '/', function (){
                        MainActivity::show();
                    });

                    $r->addRoute([
                        'GET',
                        'POST',
                    ], '/handler/', function (){
                        Activity\AddTask::handler();
                    });

                    $r->addRoute([
                        'GET',
                        'POST',
                    ], '/updatehandler/', function (){
                        Activity\UpdateTask::handler();
                    });

                });





            });
        });

        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

        switch ($routeInfo[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
                // ... 404 Not Found
                echo json_encode (array(
                    'error'=>true,
                    'message'=>'404 Not Found',
                ));
                break;
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                // ... 405 Method Not Allowed
                echo json_encode (array(
                    'error'=>true,
                    'message'=>'405 Method Not Allowed',
                ));
                break;
            case FastRoute\Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                // ... call $handler with $vars
                call_user_func_array($handler, $vars);
                break;
        }

    }
}