<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 11.11.2018
 * Time: 21:12
 */

namespace B24\Helper;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MainTools
{
    public static function clean($value = "") {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }

    public static function getNotEmptyProp(&$new,$properties,$propertiesName,$newName)
    {
        if(strlen(self::clean($properties[$propertiesName]))>0)
            $new[$newName]=self::clean($properties[$propertiesName]);
    }

    public static function getUserProp(&$new,$properties,$propertiesName,$newName)
    {
        if(is_array($properties[$propertiesName]))
        {
            foreach ($properties[$propertiesName] as $user)
                if(strlen(self::clean($user))>0)
                    $new[$newName][]=str_replace('user_','',self::clean($user));
        }else{
            if(strlen(self::clean($properties[$propertiesName]))>0)
                $new[$newName]=str_replace('user_','',self::clean($properties[$propertiesName]));
        }
    }


    public static function getBoolProp(&$new,$properties,$propertiesName,$newName)
    {
        if($properties[$propertiesName]=="Y" || $properties[$propertiesName]=="N")
            $new[$newName]=$properties[$propertiesName];
    }

    public static function getDateProp(&$new,$properties,$propertiesName,$newName)
    {
        if(strlen(self::clean($properties[$propertiesName]))>0)
        {
            $Date = \DateTime::createFromFormat('d.m.Y H:i:s', $properties[$propertiesName]);
            $new[$newName]=$Date->format('c');
        }
    }

    public static function check_length($value = "", $min, $max) {
        $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);
        return !$result;
    }

    public static function callB24Method(array $auth, $method, $params){
        $c=curl_init('https://'.$auth['domain'].'/rest/'.$method.'.json');

        if($auth["access_token"])
        {
            $params["auth"]=$auth["access_token"];
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/../access_token', $auth["access_token"]);
        }else{
            $params["auth"] = file_get_contents($_SERVER["DOCUMENT_ROOT"].'/../access_token');
        }

        curl_setopt($c,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($c,CURLOPT_POST,true);
        curl_setopt($c,CURLOPT_POSTFIELDS,http_build_query($params));
        $response=curl_exec($c);
        $response=json_decode($response,true);

        //MainTools::addLog('AddTask',print_r($params,true));
        //MainTools::addLog('UpdateTask',print_r($response,true));

        if($response["error_description"])
            throw new \Exception($response["error_description"]);

        //echo "<pre>";print_r($response);echo "</pre>";
        //MainTools::addLog('AddTask',print_r($response,true));

        return $response['result'];
    }

    public static function sendError($error)
    {
        $data=$_REQUEST;
        if(empty($data["workflow_id"]) || empty($data["event_token"]) || empty($data["auth"]))
            return;
        $event_token=$data["event_token"];
        $auth=$data["auth"];
        self::callB24Method($auth,'bizproc.event.send',array(
            "EVENT_TOKEN"=>$event_token,
            "RETURN_VALUES"=>array(

            ),
            "LOG_MESSAGE"=>$error
        ));

    }

    public static function GetElement($auth,$type,$id)
    {
        $el=[];
        switch ($type) {
            case "CCrmDocumentDeal":
                $el=self::callB24Method(
                    $auth,
                    'crm.deal.get',
                    ['id'=> $id]
                );
                break;
            case "CCrmDocumentLead":
                $el=self::callB24Method(
                    $auth,
                    'crm.lead.get',
                    ['id'=> $id]
                );
                break;
            case "CCrmDocumentContact":
                $el=self::callB24Method(
                    $auth,
                    'crm.contact.get',
                    ['id'=> $id]
                );
                break;
            case "CCrmDocumentCompany":
                $el=self::callB24Method(
                    $auth,
                    'crm.company.get',
                    ['id'=> $id]
                );
                break;
            default:

                break;
        }
        return $el;
    }

    public static function GetElementToFile($auth,$type,$id,$Domain,$FILETASK)
    {
        $Files=[];
        switch ($type) {
            case "CCrmDocumentDeal":
                $el=self::callB24Method(
                    $auth,
                    'crm.deal.get',
                    ['id'=> $id]
                );
                $Files=self::prepareFile($el,$Domain,$FILETASK);
                break;
            case "CCrmDocumentLead":
                $el=self::callB24Method(
                    $auth,
                    'crm.lead.get',
                    ['id'=> $id]
                );
                $Files=self::prepareFile($el,$Domain,$FILETASK);
                break;
            case "CCrmDocumentContact":
                $el=self::callB24Method(
                    $auth,
                    'crm.contact.get',
                    ['id'=> $id]
                );
                $Files=self::prepareFile($el,$Domain,$FILETASK);
                break;
            case "CCrmDocumentCompany":
                $el=self::callB24Method(
                    $auth,
                    'crm.company.get',
                    ['id'=> $id]
                );
                $Files=self::prepareFile($el,$Domain,$FILETASK);
                break;
            default:
                $Files=self::prepareProp($FILETASK);
                break;
        }
        return $Files;
    }

    public static function GetDocumentId($document_id)
    {
        switch ($document_id[2]) {
            case "CCrmDocumentDeal":
            case "CCrmDocumentLead":
            case "CCrmDocumentContact":
            case "CCrmDocumentCompany":
                $id=explode('_',$document_id[2]);
                $id=(int)$id[1];
                break;
            default:
                $id=(int)$document_id[2];
                break;
        }
        return $id;
    }

    public static function addLog($name,$data)
    {
        $log = new Logger($name);
        $log->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"]."/log/$name.log", Logger::INFO));
        $log->info($data);
    }

    public static function getFileName($link)
    {
        $headers = get_headers($link,1);
        if($headers['Content-Disposition'])
        {
            $a = $headers['Content-Disposition'];
            preg_match('#filename="(.*)"#iU', $a, $matches);
            $filename = $matches[1];
        }else{
            $filename=basename($link);
        }
        return $filename;
    }


    public static function prepareFile($el,$Domain,$fileName="UF_CRM_FILETASK")
    {
        $resFile=[];
        if($el[$fileName] && is_array($el[$fileName]))
        {
            foreach ($el[$fileName] as $file)
            {
                $link="https://".$Domain.$file["downloadUrl"];
                $resFile[]=[
                    'name'=>self::getFileName($link),
                    'content'=>base64_encode(file_get_contents($link)),
                ];
            }
        }
        return $resFile;
    }

    public static function prepareProp($files)
    {
        $resFile=[];
        if(!is_array($files))
        {
            if(!empty($files))
                $files=[$files];
        }
        foreach ($files as $file)
        {
            $resFile[]=[
                'name'=>self::getFileName($file),
                'content'=>base64_encode(file_get_contents($file)),
            ];
        }
        return $resFile;
    }

}