<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 08.11.2018
 * Time: 23:59
 */

namespace B24\Helper\Activity;

use B24\Helper\MainTools;


class AddTask
{

    public static function handler()
    {
        $data=$_REQUEST;

        //MainTools::addLog('AddTask',print_r($data,true));

        if(empty($data["workflow_id"]) || empty($data["auth"]))
            return;

        $event_token=$data["event_token"];
        $auth=$data["auth"];
        $properties=self::getArrParam($data["properties"]);

        $id=MainTools::GetDocumentId($data["document_id"]);

        $FILETASK="UF_CRM_FILETASK";
        if(!empty($data["properties"]["taskFile"]))
            $FILETASK=$data["properties"]["taskFile"];

        if(
            $data["properties"]["taskAutoLinkToCrmEntity"]=="Y"
            &&
            $data["document_type"]
            &&
            $data["document_id"]
            &&
            $data["document_type"][0]=="crm"
        )
        {
            $idSuffix="";
            switch ($data["document_type"][1]) {
                case "CCrmDocumentDeal":
                    $idSuffix="D_";
                    break;
                case "CCrmDocumentLead":
                    $idSuffix="L_";
                    break;
                case "CCrmDocumentContact":
                    $idSuffix="C_";
                    break;
                case "CCrmDocumentCompany":
                    $idSuffix="CO_";
                    break;
                default:

                    break;
            }
            if($id>0 && strlen($idSuffix)>0)
            {
                $newId=$idSuffix.$id;
                if(!in_array($newId,$properties["UF_CRM_TASK"]))
                    $properties["UF_CRM_TASK"][]=$newId;
            }
        }

        $taskId=(int)MainTools::callB24Method($auth,'task.item.add',["arNewTaskData"=>$properties]);

        if($taskId>0 && $id>0){

            $Files=MainTools::GetElementToFile($auth,$data["document_type"][1],$id,$data["DOMAIN"],$FILETASK);

            foreach ($Files as $file)
            {
                MainTools::callB24Method(
                    $auth,
                    'task.item.addfile',
                    [
                        'TASK_ID' => $taskId,
                        'FILE' => [
                            'NAME'=>$file['name'],
                            'CONTENT'=>$file['content']
                        ],
                    ]
                );
            }
        }
        if($taskId>0){
            MainTools::callB24Method($auth,'bizproc.event.send',array(
                "EVENT_TOKEN"=>$event_token,
                "RETURN_VALUES"=>array(
                    'TASK_ID'=>$taskId,
                ),
                "LOG_MESSAGE"=>'Задача создана'
            ));
        }
    }



    public static function getArrParam($properties)
    {
        $new=array();
        MainTools::getNotEmptyProp($new,$properties,'taskName','TITLE');

        MainTools::getUserProp($new,$properties,'taskCreatedBy','CREATED_BY');
        MainTools::getUserProp($new,$properties,'taskResponsibleID','RESPONSIBLE_ID');
        MainTools::getUserProp($new,$properties,'taskAccomplices','ACCOMPLICES');

        MainTools::getUserProp($new,$properties,'taskAuditors','AUDITORS');

        MainTools::getDateProp($new,$properties,'taskStartDatePlan','START_DATE_PLAN');
        MainTools::getDateProp($new,$properties,'taskEndDatePlan','END_DATE_PLAN');
        MainTools::getDateProp($new,$properties,'taskDeadline','DEADLINE');

        MainTools::getNotEmptyProp($new,$properties,'taskDescription','DESCRIPTION');
        MainTools::getBoolProp($new,$properties,'taskPriority','PRIORITY');
        MainTools::getNotEmptyProp($new,$properties,'taskGroupID','GROUP_ID');


        MainTools::getBoolProp($new,$properties,'taskAllowChangeDeadline','ALLOW_CHANGE_DEADLINE');
        MainTools::getBoolProp($new,$properties,'taskAllowTimeTracking','ALLOW_TIME_TRACKING');
        MainTools::getBoolProp($new,$properties,'taskTaskControl','TASK_CONTROL');
        MainTools::getBoolProp($new,$properties,'taskAddInReport','ADD_IN_REPORT');

        MainTools::getNotEmptyProp($new,$properties,'taskUfCrmTask','UF_CRM_TASK');

        if($new["PRIORITY"]=="Y")
            $new["PRIORITY"]=2;

        if($new["UF_CRM_TASK"])
            $new["UF_CRM_TASK"]=explode(';',$new["UF_CRM_TASK"]);
        else
            $new["UF_CRM_TASK"]=array();

        return $new;
    }

}