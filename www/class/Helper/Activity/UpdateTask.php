<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 12.11.2018
 * Time: 2:31
 */

namespace B24\Helper\Activity;

use B24\Helper\MainTools;

class UpdateTask
{

    public static function handler()
    {
        $data=$_REQUEST;

        //MainTools::addLog('UpdateTask',print_r($data,true));

        if(empty($data["workflow_id"]) || empty($data["auth"]))
            return;

        $auth=$data["auth"];

        $properties=$data["properties"];

        $id=MainTools::GetDocumentId($data["document_id"]);

        $FILETASK="UF_CRM_FILETASK";
        if(!empty($data["properties"]["taskFile"]))
            $FILETASK=$data["properties"]["taskFile"];

        $taskId=(int)$properties["taskID"];

        if($taskId>0 && $id>0){

            $Files=MainTools::GetElementToFile($auth,$data["document_type"][1],$id,$data["DOMAIN"],$FILETASK);

            //MainTools::addLog('AddTask',print_r($Files,true));

            foreach ($Files as $file)
            {
                MainTools::callB24Method(
                    $auth,
                    'task.item.addfile',
                    [
                        'TASK_ID' => $taskId,
                        'FILE' => [
                            'NAME'=>$file['name'],
                            'CONTENT'=>$file['content']
                        ],
                    ]
                );
            }
        }
    }

}