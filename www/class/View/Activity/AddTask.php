<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<script src="//api.bitrix24.com/api/v1/"></script>
<button onclick="installActivity();">Установить обработчик</button>
<button onclick="uninstallActivity();">Удалить обработчик</button>
<script>
    BX24.init(function()
    {

    });

    function installActivity() {
        installActivityAddTask();
        installActivityUpTask();
        addField();
    }

    function uninstallActivity() {
        uninstallAddTask();
        uninstallUpTask();
        removeField();
    }
    
    function installActivityAddTask()
    {
        var params={
            'CODE':'activity_addtask', //уникальный в рамках приложения код
            'HANDLER':'<?=BP_APP_HANDLER_HANDLER?>',
            'AUTH_USER_ID':1,
            'USE_SUBSCRIPTION':'Y', //Y - если бизнесс-процесс должен ждать ответа приложения, N - если не должен ждать
            'NAME':'Создать задачу с файлом/файлами',
            'DESCRIPTION':'Создать задачу с файлом/файлами',
            'PROPERTIES':{ //Входные данные для активити
                'taskName':{
                    'Name':'Название задачи',
                    'Type':'string',
                    'Required':'Y',
                    'Multiple':'N',
                    'Default':'',
                },
                'taskCreatedBy':{
                    'Name':'Постановщик',
                    'Type':'user',
                    'Required':'Y',
                    'Multiple':'N',
                },
                'taskResponsibleID':{
                    'Name':'Ответственный',
                    'Type':'user',
                    'Required':'Y',
                    'Multiple':'N',
                },
                'taskAccomplices':{
                    'Name':'Соисполнители',
                    'Type':'user',
                    'Required':'N',
                    'Multiple':'Y',
                },
                'taskStartDatePlan':{
                    'Name':'Начало',
                    'Type':'datetime',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskEndDatePlan':{
                    'Name':'Окончание',
                    'Type':'datetime',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskDeadline':{
                    'Name':'Крайний срок',
                    'Type':'datetime',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskDescription':{
                    'Name':'Описание задачи',
                    'Type':'text',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskPriority':{
                    'Name':'Важность',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskGroupID':{
                    'Name':'ID Проектной группы',
                    'Type':'int',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskAllowChangeDeadline':{
                    'Name':'Разрешить ответственному менять крайний срок',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskAllowTimeTracking':{
                    'Name':'Включить учет времени по задаче',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskTaskControl':{
                    'Name':'Принять работу после завершения задачи',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskAddInReport':{
                    'Name':'Включить задачу в отчет по эффективности',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskUfCrmTask':{
                    'Name':'ID Сущности CRM (D_ - сделка, L_ - лид, C_ - контакт, CO_ - компания)',
                    'Type':'string',
                    'Required':'N',
                    'Multiple':'N',
                    'Default':'',
                },
                'taskAuditors':{
                    'Name':'Наблюдатели',
                    'Type':'user',
                    'Required':'N',
                    'Multiple':'Y',
                },
                'taskAutoLinkToCrmEntity':{
                    'Name':'Привязать к текущей сущности CRM',
                    'Type':'bool',
                    'Required':'N',
                    'Multiple':'N',
                },
                'taskFile':{
                    'Name':'Название свойства с файлом',
                    'Type':'string',
                    'Required':'N',
                    'Multiple':'N',
                },
            },
            'RETURN_PROPERTIES':{ //данные, которые активити будет возвращать бизнес-процессу
                'TASK_ID':{
                    'Name':'TASK_ID',
                    'Type':'int',
                    'Required':'Y',
                    'Multiple':'N',
                },
            }
        };

        BX24.callMethod(
            'bizproc.activity.add',
            params,
            function(result)
            {
                if (result.error())
                    alertify.alert('Error: '+result.error());
                else
                    alertify.alert('Installation successfully completed');
            }
        );
    }

    function uninstallAddTask(){
        var params={
            'CODE':'activity_addtask'
        };

        BX24.callMethod(
            'bizproc.activity.delete',
            params,
            function (result)
            {
                if (result.error())
                    alertify.alert('Error: '+result.error());
                else
                    alertify.alert('Uninstallathion successfully completed');
            }
        );
    }

    function installActivityUpTask()
    {
        var params={
            'CODE':'activity_uptask', //уникальный в рамках приложения код
            'HANDLER':'<?=BP_APP_HANDLER_HANDLER_UPDATE?>',
            'AUTH_USER_ID':1,
            'USE_SUBSCRIPTION':'N', //Y - если бизнесс-процесс должен ждать ответа приложения, N - если не должен ждать
            'NAME':'Добавить файл/файлы к задаче',
            'DESCRIPTION':'Добавить файл/файлы к задаче',
            'PROPERTIES':{ //Входные данные для активити
                'taskID':{
                    'Name':'ID задачи',
                    'Type':'int',
                    'Required':'Y',
                    'Multiple':'N',
                    'Default':'',
                },
                'taskFile':{
                    'Name':'Название свойства с файлом',
                    'Type':'string',
                    'Required':'N',
                    'Multiple':'N',
                },
            }
        };

        BX24.callMethod(
            'bizproc.activity.add',
            params,
            function(result)
            {
                if (result.error())
                    alertify.alert('Error: '+result.error());
                else
                    alertify.alert('Installation successfully completed');
            }
        );
    }

    function uninstallUpTask(){
        var params={
            'CODE':'activity_uptask'
        };

        BX24.callMethod(
            'bizproc.activity.delete',
            params,
            function (result)
            {
                if (result.error())
                    alertify.alert('Error: '+result.error());
                else
                    alertify.alert('Uninstallathion successfully completed');
            }
        );
    }

    function addField() {
        var param={
            "FIELD_NAME":"filetask",
            "EDIT_FORM_LABEL":"filetask",
            "LIST_COLUMN_LABEL":"filetask",
            "USER_TYPE_ID":"file",
            "MULTIPLE":"Y",
            "XML_ID":"filetask"
        };
        BX24.callMethod("crm.deal.userfield.add",{fields:param});
        BX24.callMethod("crm.lead.userfield.add",{fields:param});
        BX24.callMethod("crm.contact.userfield.add",{fields:param});
        BX24.callMethod("crm.company.userfield.add",{fields:param});
    }
    
    function removeField() {
        var param={
            order: {},
            filter: {"XML_ID":"filetask"}
        };
        BX24.callMethod(
            "crm.deal.userfield.list",param,
            function(result)
            {
                if(!result.error())
                    BX24.callMethod("crm.deal.userfield.delete",{id: result.data()[0].ID});
            }
        );
        BX24.callMethod(
            "crm.lead.userfield.list",param,
            function(result)
            {
                if(!result.error())
                    BX24.callMethod("crm.lead.userfield.delete",{id: result.data()[0].ID});
            }
        );
        BX24.callMethod(
            "crm.contact.userfield.list",param,
            function(result)
            {
                if(!result.error())
                    BX24.callMethod("crm.contact.userfield.delete",{id: result.data()[0].ID});
            }
        );
        BX24.callMethod(
            "crm.company.userfield.list",param,
            function(result)
            {
                if(!result.error())
                    BX24.callMethod("crm.company.userfield.delete",{id: result.data()[0].ID});
            }
        );
    }
    
</script>
</body>
</html>